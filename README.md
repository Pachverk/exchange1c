# Exchange1C
Еще не все методы дописаны

##### Установка
    composer require pachverk/exchange1c

##### Пример использования
    // $exchange->setDataPath('webdata/000000001/import.xml');
    $exchange->setDataPath('webdata');
    
    $exchange->scanData(); // Процес проверки всех данных указаных с метода setDataPath
    dump($exchange->getListExchange()); // Получение списка файлов для обмена в нужном порядке
    dump($exchange->getCatalogs()); // Информация о каталогах
    dump($exchange->getPrices()); // Информация о ценах
    dump($exchange->getMeasure()); // Информация о единицах измерений
    dump($exchange->getSections()); // Информация о разделах
    dump($exchange->getElements()); // Информация товарах
    dump($exchange);
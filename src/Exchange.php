<?php


namespace Pachverk;


class Exchange
{
    /**
     * Список файлов для выгрузки, если есть таковые в переменной <br>
     * Можно определить или получить через: <br>
     * @uses \Pachverk\Exchange::getListExchangeFromData() Получение результатов сканирования
     * @uses \Pachverk\Exchange::scanData() Проверка содежимого с даты
     * @var array Список файлов XML в нужном порядке для выгрузки
     */
    public $xmlFileslist = [];

    /** @var \XMLReader */
    // private $xml;

    /** @var \SplFileInfo Путь к источнику обмена */
    public $dataPath;

    /** @var array Место хранение всей информации по полученой с XML файлов */
    public $structure;

    private $catalogs = [];
    private $properties = [];
    private $sections = [];
    private $elements = [];
    private $prices = [];
    private $offers = [];
    private $measure = [];
    private $procFile = null;

    private $mapElements;

    /**
     * Определение источника с которого будут проводится манипляции
     * @param string $path
     * @return $this
     */
    public function setDataPath(string $path)
    {
        $this->dataPath = new \SplFileInfo($path);
        return $this;
    }

    /**
     * Вернет список файлов в нужно очередности для более удобной обработки
     * @return array Массив Строчных путей к файлам
     * @throws \Exception
     * @uses \Pachverk\Exchange::$xmlFileslist
     */
    public function getListExchange(): array
    {
        $list = [];
        if (!empty($this->xmlFileslist)) {
            /** @var \SplFileInfo $file */
            foreach ($this->xmlFileslist as $file) {
                $list[] = $file->getRealPath();
            }
        }
        return $list;
    }

    /**
     * Сканирует содержимое всех XML файлов
     * @return $this
     * @throws \Exception
     */
    public function scanData(): self
    {
        $funcParams = [
            'offers'            => true,
            'catalog'           => true,
            'catalogPrices'     => true,
            'catalogSections'   => true,
            'catalogMeasure'    => true,
            'catalogProperties' => true,
            'catalogElements'   => true,
        ];

        /**
         * @var \SplFileInfo $xmlFile
         */

        $this->getListExchangeFromData();
        foreach ($this->getListExchangeFromData() as $xmlFile) {
            $this->getXmlFromFile($xmlFile);
        }

        $this->catalogs =
        $this->properties =
        $this->sections =
        $this->elements =
        $this->measure =
        $this->prices =
        $this->offers = [];

        if (empty($this->catalogs)) {
            foreach ($this->structure as $fileName => $data) {

                $this->setCurrentFile($fileName);

                // Определение каталога
                if ($funcParams['catalog'] && isset($data['Каталог'])) {

                    $id = $data['Каталог']['Ид'];
                    $catalogInfo = $data['Каталог'];

                    $this->catalogs[$id]['id'] = $id;
                    $this->catalogs[$id]['name'] = $catalogInfo['Наименование'];
                    $this->catalogs[$id]['description'] = $catalogInfo['Описание'];
                    $this->catalogs[$id]['class'] = $catalogInfo['ИдКлассификатора'];
                    $this->catalogs[$id]['onlyChanges'] = (bool)$catalogInfo['@attributes']['СодержитТолькоИзменения'];

                    // Более подробная информация
                    if (isset($data['Классификатор'])) {
                        $catalog = $data['Классификатор'];

                        // Разделы
                        // todo Обработка массива в исходном виде
                        if ($funcParams['catalogSections']
                            && isset($catalog['Группы']['Группа'])
                            && !empty($catalog['Группы']['Группа'])
                        ) {
                            $this->sections[$id]['Группы'] = $catalog['Группы'];
                        }

                        // Свойства
                        // todo Обработка массива в исходном виде
                        if ($funcParams['catalogProperties']
                            && isset($catalog['Свойства']['Свойство'])
                            && !empty($catalog['Свойства']['Свойство'])
                        ) {
                            foreach ($catalog['Свойства']['Свойство'] as $prop) {
                                $this->properties[$id][$prop['Ид']] = $prop;
                            }
                        }

                        // Типы цен
                        // todo Обработка массива в исходном виде
                        if ($funcParams['catalogPrices']
                            && isset($catalog['ТипыЦен']['ТипЦены'])
                            && !empty($catalog['ТипыЦен']['ТипЦены'])
                        ) {
                            if (isset($catalog['ТипыЦен']['ТипЦены']['Ид'])) {
                                $this->prices[$id][$catalog['ТипыЦен']['ТипЦены']['Ид']] = $catalog['ТипыЦен']['ТипЦены'];
                            } else {
                                foreach ($catalog['ТипыЦен']['ТипЦены'] as $price) {
                                    $this->prices[$id][$price['Ид']] = $price;
                                }
                            }
                        }

                        // Единицы измерений
                        // todo Обработка массива в исходном виде
                        if ($funcParams['catalogMeasure']
                            && isset($catalog['ЕдиницыИзмерения']['ЕдиницаИзмерения'])
                            && !empty($catalog['ЕдиницыИзмерения']['ЕдиницаИзмерения'])
                        ) {
                            if (isset($catalog['ЕдиницыИзмерения']['ЕдиницаИзмерения']['Ид'])) {
                                $this->measure[$id][$catalog['ЕдиницыИзмерения']['ЕдиницаИзмерения']['Ид']] = $catalog['ЕдиницыИзмерения']['ЕдиницаИзмерения'];
                            } else {
                                foreach ($catalog['ЕдиницыИзмерения']['ЕдиницаИзмерения'] as $price) {
                                    $this->measure[$id][$price['Ид']] = $price;
                                }
                            }
                        }
                    }

                    if ($funcParams['catalogElements']
                        && isset($data['Каталог']['Товары']['Товар'])
                        && !empty($data['Каталог']['Товары']['Товар'])
                    ) {

                        foreach ($data['Каталог']['Товары']['Товар'] as $element) {
                            $this->setProduct($element, $id);
                        }

                        // todo нужно снести
                        if (0) {
                            $this->elements[$id] = array_map((is_callable($this->mapElements))
                                ? $this->mapElements
                                /** @uses \Pachverk\Exchange::mapElements() */
                                : [$this, 'mapElements'],
                                $this->elements[$id]
                            );
                        }
                    }
                }

                // Обработка торговых предложений
                if ($funcParams['offers'] && isset($data['ПакетПредложений'])) {
                    $id = $data['ПакетПредложений']['Ид'];
                    $catalogInfo = $data['ПакетПредложений'];
                    $this->catalogs[$id]['id'] = $id;
                    $this->catalogs[$id]['name'] = $catalogInfo['Наименование'];
                    $this->catalogs[$id]['catalogID'] = $catalogInfo['ИдКаталога'];
                    $this->catalogs[$id]['class'] = $catalogInfo['ИдКлассификатора'];
                    $this->catalogs[$id]['onlyChanges'] = (bool)$catalogInfo['@attributes']['СодержитТолькоИзменения'];
                    if (isset($catalogInfo['Описание']))
                        $this->catalogs[$id]['description'] = $catalogInfo['Описание'];

                    if (isset($data['ПакетПредложений']['Предложения']['Предложение'])
                        && !empty($data['ПакетПредложений']['Предложения']['Предложение'])
                    ) {
                        foreach ($data['ПакетПредложений']['Предложения']['Предложение'] as $element) {
                            $this->setProduct($element, $id);
                            // $this->elements[$id][$element['Ид']]['file'] = new \SplFileInfo($fileName);
                            // $this->elements[$id][$element['Ид']] = array_merge(
                            //     $this->elements[$id][$element['Ид']] ?? [],
                            //     $element
                            // );
                        }
                    }

                    // Более подробная информация
                    if (isset($data['Классификатор'])) {
                        $catalog = $data['Классификатор'];

                        // Свойства
                        // todo Обработка массива в исходном виде
                        if (isset($catalog['Свойства']['Свойство'])
                            && !empty($catalog['Свойства']['Свойство'])
                        ) {
                            $this->setProperty($catalog['Свойства']['Свойство'], $id);
                        }
                    }

                }

                // Поймать свойства
                if (1) {
                    if (isset($data['ПакетПредложений'])) {
                        $id = $data['ПакетПредложений']['Ид'];

                        if (isset($data['Классификатор']['Свойства']['Свойство'])
                        && !empty($data['Классификатор']['Свойства']['Свойство'])) {
                            $this->setProperty($data['Классификатор']['Свойства']['Свойство'], $id);
                        }
                    }
                }

            }
        }

        return $this;
    }

    /**
     * Вернет список файлов в нужно очередности для более удобной обработки
     * @return array Массив объектов \SplFileInfo
     * @throws \Exception
     * @uses \Pachverk\Exchange::$xmlFileslist
     */
    public function getListExchangeFromData()
    {
        $this->xmlFileslist = [];
        $this->validator();

        if ($this->dataPath->isFile()) {
            if ($this->dataPath->getExtension() != 'xml') {
                throw new \Exception("File must by xml <{$this->dataPath->getRealPath()}>");
            }
            $this->xmlFileslist[] = $this->dataPath;

        } elseif ($this->dataPath->isDir()) {
            $this->getListExchangeByDir($this->dataPath);
        }

        return $this->xmlFileslist;
    }

    /**
     * Конвертирует объект в массив
     * @param       $arrObjData
     * @param array $arrSkipIndices
     * @return array
     */
    public static function objectsIntoArray($arrObjData, $arrSkipIndices = [])
    {
        $arrData = [];

        if (is_object($arrObjData)) {
            $arrObjData = get_object_vars($arrObjData);
        }

        if (is_array($arrObjData)) {
            foreach ($arrObjData as $index => $value) {
                if (is_object($value) || is_array($value)) {
                    $value = self::objectsIntoArray($value, $arrSkipIndices);
                }
                if (in_array($index, $arrSkipIndices)) {
                    continue;
                }
                $arrData[$index] = $value;
            }
        }
        return $arrData;
    }

    /**
     * The method runs through the directory and determines the list of queues to call
     * @param \SplFileInfo $dir
     * @throws \Exception
     */
    private function getListExchangeByDir(\SplFileInfo $dir)
    {
        if (!$dir->isDir()) {
            throw new \Exception('Something incomprehensible flew into the processing of the directory. It is necessary to pay attention to the logic of work');
        }

        $main =
        $propOffers = $propDiscountCard =
        $discountCard = $rests = $prices = $products = $offers = [];

        /** @var \SplFileInfo $file */
        foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir->getRealPath())) as $file) {
            if (!in_array($file->getExtension(), ['xml'])) continue;

            $relativePath = str_replace($this->dataPath, '', $file->getPath());

            // Got files related to filling elements
            if (strpos($relativePath, '/goods/') !== false) {
                $type = explode('___', $file->getFilename())[0];
                switch ($type) {
                    case 'import':
                        $products[] = $file;
                        break;
                    case 'offers':
                        $offers[] = $file;
                        break;
                    case 'prices':
                        $prices[] = $file;
                        break;
                    case 'rests':
                        $rests[] = $file;
                        break;
                }
            } elseif (strpos($relativePath, 'properties') !== false) {
                $type = explode('___', $file->getFilename())[0];
                switch ($type) {
                    case 'offers':
                        $propOffers[] = $file;
                        break;
                    case 'discountCard':
                        $propDiscountCard[] = $file;
                        break;
                }
            } else {
                $main[] = $file;
            }
        }

        $this->xmlFileslist = array_merge(
            $this->xmlFileslist,
            $main,
            $propOffers, $propDiscountCard,
            $products, $offers, $prices, $rests
        );
    }


    public function getCatalogs()
    {
        return $this->catalogs;
    }

    public function getElements(string $catalogID = null): array
    {
        return (!empty($catalogID)) ? $this->elements[$catalogID] : $this->elements;
    }

    protected function mapElements($element)
    {

        // Видоизменение свойст
        if (isset($element['ЗначенияСвойств']['ЗначенияСвойства'])) {
            $element['properties'] = [];
            foreach ($element['ЗначенияСвойств']['ЗначенияСвойства'] as $prop) {
                $element['properties'][$prop['Ид']] = $prop;
            }
            unset($element['ЗначенияСвойств']['ЗначенияСвойства']);
        }

        // Значения реквизитов
        if (isset($element['ЗначенияРеквизитов']['ЗначениеРеквизита'])) {
            $element['valueRequisites'] = $element['ЗначенияРеквизитов']['ЗначениеРеквизита'];
            unset($element['ЗначенияРеквизитов']['ЗначениеРеквизита']);
        }

        return $element;
    }

    public function getProperties($id = null): array
    {
        return (isset($id)) ? $this->properties[$id] : $this->properties;
    }

    public function getSections(): array
    {
        return $this->sections;
    }

    public function getOffers(): array
    {
    }

    public function getPrices(): array
    {
        return $this->prices;
    }

    public function getMeasure(): array
    {
        return $this->measure;
    }

    private function getRests(): array
    {
    }


    /**
     * @return true|\Exception
     * @throws \Exception
     */
    private function validator(): bool
    {
        if (!isset($this->dataPath)) {
            throw new \Exception("Empty data path, Please use setDataPath" . PHP_EOL);
        }

        if (!$this->dataPath->isDir() && !$this->dataPath->isFile()) {
            throw new \Exception("Data path must by file or directory");
        }

        if (!class_exists(\XMLReader::class)) {
            throw new \Exception("Class not found (" . \XMLReader::class . ")");
        }

        return true;
    }

    private function setCurrentFile($path) {
        $this->procFile = new \SplFileInfo($path);
    }

    private function getCurrentFile() {
        return $this->procFile;
    }

    /*private function getNextElement()
    {
        while ($this->xml->read()) {
            if ($this->xml->nodeType === \XMLReader::ELEMENT) {
                return $this->xml;
            }
        }
        return null;
    }*/

    /**
     * Информация получения после парсинга самого файла
     * @param \SplFileInfo $xmlPath
     * @return array
     */
    private function getXmlFromFile(\SplFileInfo $xmlPath)
    {
        $xmlObj = simplexml_load_file($xmlPath->getRealPath());
        $this->structure[$xmlPath->getRealPath()] = self::objectsIntoArray($xmlObj);
        return $this->structure[$xmlPath->getRealPath()];
    }





    /**
     * Обработка свойств которые есть в выгрузке <br>
     * Сохранение их в <br> $this->properties[$idCatalog][$idProp] = $prop
     * @param array $arData
     * @param string $idCatalog Идентификатор каталога
     */
    protected function setProperty(array $arData, string $idCatalog='') {
        if (isset($arData['Ид'])) {
            $this->properties[$idCatalog][$arData['Ид']] = $arData;
        } else {
            foreach ($arData as $prop) {
                $this->properties[$idCatalog][$prop['Ид']] = $prop;
            }
        }
    }

    /**
     * Обработка товаров которые есть в выгрузке <br>
     * Сохранение их в <br> $this->elements[$idCatalog][$idProduct] = $product
     * @param array  $element
     * @param string $idCatalog
     */
    protected function setProduct(array $element, string $idCatalog='') {
        if (!isset($this->elements[$idCatalog][$element['Ид']]))
            $this->elements[$idCatalog][$element['Ид']] = [];
        $element['file'] = $this->getCurrentFile();
        $this->elements[$idCatalog][$element['Ид']] = array_merge(
            $this->elements[$idCatalog][$element['Ид']],
            $element
        );
    }
}
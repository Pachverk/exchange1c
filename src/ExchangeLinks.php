<?php


namespace Pachverk;


use Illuminate\Support\Facades\DB;

class ExchangeLinks
{
    protected $integrationID;

    /**
     * @param string|null $integrationID
     * @return ExchangeLinks
     */
    public static function instance(string $integrationID=null) {
        static $instance;
        if (!isset($instance)) {
            $instance = new self();
            $instance->setIntegrationID($integrationID);
        }
        return $instance;
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     * @throws \Exception
     */
    protected function getDBIntegrations() {
        $tableName = env('TABLE_INTEGRATIONS');
        if (empty($tableName)) {
            throw new \Exception('Please set param "TABLE_INTEGRATIONS" to env');
        }
        return DB::table($tableName);
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     * @throws \Exception
     */
    protected function getDBIntegrationRelations()
    {
        $tableName = env('TABLE_INTEGRATION_RELATIONS');
        if (empty($tableName)) {
            throw new \Exception('Please set param "TABLE_INTEGRATION_RELATIONS" to env');
        }
        return DB::table($tableName);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    protected function getIntegrationID() {
        if (!isset($this->integrationID))
            throw new \Exception('Need setIntegrationID()');
        return $this->integrationID;
    }

    public function setIntegrationID(string $id) {
        $this->integrationID = $id;
        return $this;
    }

    /**
     * @param $entity
     * @param $internalId
     * @param $externalId
     * @throws \Exception
     */
    public function setRelation($entity, $internalId, $externalId) {
        $data = [
            'entity'         => $entity,
            'integration_id' => $this->getIntegrationID(),
            'external_id'    => $externalId,
            'internal_id'    => $internalId,
        ];
        $this->getDBIntegrationRelations()->updateOrInsert($data, $data);
    }

    /**
     * Поиск внутреннего ID по входному внешнему
     * @param string $entity
     * @param int|array $externalId
     * @return array|int|null
     * @throws \Exception
     */
    public function getIdByExternal(string $entity, $externalId) {
        $relation = $this->getDBIntegrationRelations()->where([
            'integration_id' => $this->getIntegrationID(),
            'entity' => $entity
        ])->whereIn('external_id', (is_array($externalId)) ? $externalId : [$externalId])
            ->select('internal_id')
            ->get();

        if (is_array($externalId)) {
            return array_column($relation->toArray(), 'internal_id');
        } else {
            return $relation->first()->internal_id ?? null;
        }
    }

    public function getExternalId(string $entity, $internalId) {
        $relation = $this->getDBIntegrationRelations()->where([
            'integration_id' => $this->getIntegrationID(),
            'entity' => $entity
        ])->whereIn('internal_id', (is_array($internalId)) ? $internalId : [$internalId])
            ->select('external_id')
            ->get();

        if (is_array($internalId)) {
            return $relation->toArray();
        } else {
            return $relation->first()->internal_id ?? null;
        }
    }
}